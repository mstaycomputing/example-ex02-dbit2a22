import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'find'
})
export class FindPipe implements PipeTransform {

  // this pipe takes in 3 parameters
  // value: anything (in this case it's a list of Friend objects
  // propertyName: a string indicating what property of a Friend we're filtering by (e.g. name, or email, or contact etc..)
  // searchStr: a string that is what we're trying to filtering for (e.g. "John")
  transform(value: any, propertyName: string, searchStr: string): any {

    // if there's no Friends in the list (value) at all, OR if there's nothing typed in the search bar (searchStr)
    // we simply return the whole list (value) untouched
    if (value.length === 0 || searchStr.trim().length === 0) {
      return value;
    }

    // this is the array for us to collect the filtered results
    let resultArray = [];

    // convert what we're filtering for to lower case to make our search case-insesitive
    searchStr = searchStr.toLowerCase();

    // loop through each Friend object (we're calling it elem here)
    for (const elem of value) {
      // convert the name of the Friend object to lower case to make our search case-insensitive
      const str = elem[propertyName].toLowerCase();

      // a technique to help us check if searchStr exists in str
      // it returns -1 if searchStr is NOT found in str
      // it returns a number that is 0 or above if searchStr is found in str
      if (str.indexOf(searchStr) != -1) {
        // found, hence we add it to our results
        resultArray.push(elem);
      }
    }

    // remember to return your filtered Friend list!
    return resultArray;
  }

}
