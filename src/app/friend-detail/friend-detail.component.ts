import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FriendsService } from '../friend/friends.service';
import { Friend } from '../friend/friend.model';

@Component({
  selector: 'app-friend-detail',
  templateUrl: './friend-detail.component.html',
  styleUrls: ['./friend-detail.component.css']
})
export class FriendDetailComponent implements OnInit {

  thisFriend: Friend = new Friend(0, '', '', '', ''); // dummy friend

  // ActivatedRoute is used to grab URL parameters
  // FriendsService is used to handle any operations related to the friendList
  // Router is used to handle redirection
  constructor(private activatedRoute: ActivatedRoute,
              private friendsService: FriendsService,
              private router: Router) { }

  ngOnInit() {

    // grab a copy of the fr_id attached to the URL (this is defined in app-routing.module.ts)
    // it's the number after the slash (/) when we access friend-detail (e.g. /friend-detail/1)
    console.log(this.activatedRoute.snapshot.params['fr_id']);

    // we pass the fr_id from the URL directly into the service to try to obtain the Friend object
    this.friendsService.getFriend(this.activatedRoute.snapshot.params['fr_id'])
      .subscribe(
        (result) => { // result is a Friend
          this.thisFriend = result;

          // if the service threw out undefined, we know the fr_id doesn't exist
          if (this.thisFriend === undefined) {
            // hence, redirect to not-found
            this.router.navigate(['not-found']);
          }
        }
      )
  
  }

  // function to handle the "Back" button click from the friend-detail HTML
  onBackClick() {

    // redirects user back to the /friend page
    this.router.navigate(['friend']);
  }

  // function to handle the "Edit" button click from the friend-detail HTML
  onEditClick() {
    // redirects to the /friend-edit page with the corresponding fr_id
    this.router.navigate(['friend-edit', this.thisFriend.fr_id]);
  }

}
