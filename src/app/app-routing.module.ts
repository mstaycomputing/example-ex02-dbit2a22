import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from "./home/home.component";
import { FriendComponent } from "./friend/friend.component";
import { HeaderComponent } from "./header/header.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FriendDetailComponent } from './friend-detail/friend-detail.component';

import { AuthGuardService } from "./auth/auth-guard.service";
import { FriendEditComponent } from './friend/friend-edit/friend-edit.component';

// these routes must be defined for them to be considered valid URLs for your app
// any routes not defined here will result in 404 (not found) error
const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'friend', canActivate: [AuthGuardService], component: FriendComponent},
  {path: 'contactus', component: ContactUsComponent},
  {path: 'not-found', component: PageNotFoundComponent},
  {path: 'friend-detail/:fr_id', component: FriendDetailComponent},
  {path: 'friend-edit/:fr_id', component: FriendEditComponent},
  {path: '**', redirectTo: '/not-found'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
