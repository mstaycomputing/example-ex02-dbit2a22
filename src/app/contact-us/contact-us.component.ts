import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  // refers to the  whole <form> object in the HTML
  contactForm : FormGroup;

  // indicates if the form was successfully submitted or not
  // helps us to decide whether to display the form, or the summary section
  submitted = false;

  // Additional exercise: simplistic way of checking for valid phone number characters
  // to be used in phoneCharacters Validator function later 
  acceptedPhoneCharacters = ['0','1','2','3','4','5','6','7','8','9','(',')','+','-',' '];

  // some sample "blacklisted" emails which we don't allow to send us a contact message
  // to be used in inEmailBlackList Validator function later 
  emailBlackList = ['test@test.com', 'temp@temp.com'];

  constructor() { }

  ngOnInit() {
    // for Validators which require access to variables defined above, we need to add .bind(this)
    this.contactForm = new FormGroup({
      'username' : new FormControl(null, [Validators.required, Validators.minLength(3), this.blankSpaces]),
      'useremail' : new FormControl(null, [Validators.required, Validators.email, this.inEmailBlackList.bind(this)]),
      'userphone' : new FormControl(null, [Validators.required, this.phoneCharacters.bind(this)]),
      'usermessage' : new FormControl(null, [Validators.required]),
    });
  }

  // self-defined Validator to check if a string consists of only spaces
  blankSpaces(control: FormControl) : {[s: string]: boolean} {
    if (control.value != null && control.value.trim().length === 0) {

      // return type is an object containing a key with the name of the error
      // this error name is self-defined (as long as you check for the same error name in your HTML code)
      return {'blankSpaces' : true}
    }
    return null;
  }

  // self-defined Validator to check if a phone number input has invalid characters 
  phoneCharacters(control: FormControl) : {[s:string]: boolean} {
    // loop through each character
    for (let char of String(control.value)) {

      // the moment we detect that a character isn't found in the defined list, we conclude there's an error input
      if (this.acceptedPhoneCharacters.indexOf(char) == -1) {

        // return type is an object containing a key with the name of the error
        // this error name is self-defined (as long as you check for the same error name in your HTML code)
        return {'phoneCharacters' : true}
      }
    }
    return null;
  }

  // self-defined Validator to check if an email is blacklist (as defined in list above)
  inEmailBlackList(control: FormControl): {[s: string]: boolean} {
    if (this.emailBlackList.indexOf(control.value) !== -1) {

      // return type is an object containing a key with the name of the error
      // this error name is self-defined (as long as you check for the same error name in your HTML code)
      return {'emailBlackListed': true};
    }
    return null;
  }


  // function that responds to "Submit" button click in HTML
  onSubmit() {
    console.log(this.contactForm);

    // we change submitted to true so that
    // the summary section will show (refer to *ngIf in the HTML code)
    // and the form will hide (refer to *ngIf in the HTML code)
    this.submitted = true;
  }

  // function that responds to "Back" button click in HTML
  onBackClick() {
    // we change submitted to false so that
    // the summary section will hide (refer to *ngIf in the HTML code)
    // and the form will show again (refer to *ngIf in the HTML code)
    this.submitted = false;
    this.contactForm.reset();
  }

}
