import { Component, OnInit } from '@angular/core';
import { Friend } from './friend.model';
import { FriendsService } from "./friends.service";

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css'],
  providers: []
})
export class FriendComponent implements OnInit {

  // Declaring and initialising friendList (data will be inserted later)
  friendList : Friend [] = [];

  /////// Old way of maintaining a friendList:                         ////////
  //////  As a public list of Friend objects (accessible from outside) ////////
  // friendList : Friend [] = [
  //   new Friend("John Lim", "john@gmail.com", "91234567"),
  //   new Friend("Mary Tan", "mary@hotmail.com", "81234567")
  // ];

  // Inject the FriendsService into this component
  constructor(private friendsService: FriendsService) { }

  // This method executes only once upon the component loading
  ngOnInit() {
    // Grab the current list of Friends from the friendsService
    this.friendList = this.friendsService.getFriends();

    // Creates a "subscription" to the "loudhailer" (EventEmitter) from the friendsService
    // Once the "loudhailer" broadcasts something,
    // we react by grabbing the most current list of Friends from the friendsService 
    this.friendsService.friendAdded.subscribe( () => {
      this.friendList = this.friendsService.getFriends();
    });

    // same as above, except we subscribe to the event when friend was deleted instead of added
    this.friendsService.friendDeleted.subscribe( () => {
      this.friendList = this.friendsService.getFriends();
    });

    // server call to GET list of Friends
    // returns us an Observable
    this.friendsService.loadFriends()
      .subscribe((result) => { // trigger the Observable (result is a list of Friends)
        // sync up the list we got from the server with the list in this component
        this.friendList = this.friendsService.getFriends();
      });
  }

  /////// Old way of adding a friend:                                                  ///////
  /////// "catching" the Friend object that was emitted from the friend-add.component  ///////
  ///////  and pushing it into the public friendList maintained here                   ///////
  // onFriendAdded(newFriendInfo) {
  //   console.log("Friend component");
  //   console.log(newFriendInfo);
  //   this.friendList.push(newFriendInfo);
  //   console.log(this.friendList);
  // }

}
