import { Component, OnInit } from '@angular/core';
import { Friend } from '../friend.model';
import { FriendsService } from "../friends.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-friend-edit',
  templateUrl: './friend-edit.component.html',
  styleUrls: ['./friend-edit.component.css']
})
export class FriendEditComponent implements OnInit {

  // we are using [(ngModel)] to bind the friend's info to the form
  inputInfo: Friend = new Friend(0, '', '', '', '');

  constructor(private friendsService: FriendsService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    // server call to GET a single friend (returns us an Observable)
    this.friendsService.getFriend(this.activatedRoute.snapshot.params['fr_id'])
      .subscribe( // trigger the Observable
        (result) => { // result is a Friend
          // sync up the Friend we got from the server
          // to inputInfo which is binded to the form
          // so that the form will be populated with the Friend's info
          this.inputInfo = result; 
        }
      )
  }

  onEditFriend() {
    // server call to PUT a friend with a particular id (returns us an Observable)
    this.friendsService.updateFriend(this.inputInfo)
      .subscribe( // trigger the Observable
        (result) => { // result is a JSON object containing success and message
          console.log(result); // success will be false if nothing was updated

          // navigate back to the friend-detail page of the friend
          this.router.navigate(['friend-detail', this.inputInfo.fr_id]);
        }
      )
  }

}
