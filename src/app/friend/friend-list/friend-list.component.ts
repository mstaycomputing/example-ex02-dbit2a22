import { Component, OnInit, Input } from '@angular/core';
import { Friend } from '../friend.model';
import { FriendsService } from '../friends.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {

  // property-binded (refer to friend.component.html)
  // gets its data from the friend component's friendList
  @Input() myFriends : Friend [];

  searchStr = "";

  // Additional exercise: using ngFor with simple lists
  heroes = ['Windstorm', 'Bombasto', 'Magneta', 'Tornado', 'Powerpuff Girl'];

  constructor(private friendsService: FriendsService,
              private router: Router) { }

  ngOnInit() {
  }

  // a function to respond to the "Delete" button click from the HTML
  onDeleteFriend(index) {
    // make a call to the service to delete the friend
    // which will in turn trigger a server DELETE call
    this.friendsService.deleteFriend(index);
  }

  // a function to respond to the "More" button click from the HTML
  onViewMore(fr_id) {
    // navigate us to the friend-detail/* page, where * refers to the fr_id of the Friend
    this.router.navigate(['friend-detail', fr_id]);
  }
  
}
