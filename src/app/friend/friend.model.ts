export class Friend {
    // public name: string;
    // public contact: string;
    // public email: string;

    // short-hand way of creating and initialising variables
    // add access modifier (public / private) before each parameter
    constructor(public fr_id: number, public name: string, public email: string, public contact: string, public address: string) {
        // this.name = name;
        // this.email = email;
        // this.contact = contact;
    }
}