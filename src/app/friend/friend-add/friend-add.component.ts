import { Component, OnInit, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Friend } from '../friend.model';
import { FriendsService } from "../friends.service";

@Component({
  selector: 'app-friend-add',
  templateUrl: './friend-add.component.html',
  styleUrls: ['./friend-add.component.css']
})
export class FriendAddComponent implements OnInit {
  // 1st way to access data from HTML template: ngModel (data binding)
  inputInfo: Friend = new Friend(0, '', '', '', '');

  // 3rd way to access data from HTML template: Local Reference (#) with ViewChild
  @ViewChild('inputEmail', {static:false}) inputEmail: ElementRef;

  // Creates a "loudhailer" (EventEmitter) that emits a Friend object
  // This "loudhailer" will be an Output from this (friend-add) component
  @Output() friendAdded = new EventEmitter<Friend>();

  // Inject the FriendsService into this component
  // This is the same instance that was provided in the friend component
  constructor(private friendsService: FriendsService) { }

  ngOnInit() {
  }

  onAddFriend(inputName: HTMLInputElement) {

    // additional exercise solution: only add friend when name is not empty
    if (inputName.value != "") {

      /////// Old way of pushing new Friend into the list:   ///////
      /////// emitting it into the parent (friend) component ///////
      // this.friendAdded.emit(new Friend(
      //   inputName.value,
      //   this.inputEmail.nativeElement.value,
      //   this.inputInfo.contact,
      //   this.inputInfo.address
      // ));

      /////// Old way of pushing new Friend into the list:          ///////
      /////// pushing it into the directly into the friendsService /////// 
      this.friendsService.addFriend(new Friend(
        this.friendsService.getFriends().length + 1, // THIS ID WILL BE IGNORED BY THE SERVER
        inputName.value, // 2nd way of accessing data: local reference with input parameter
        this.inputEmail.nativeElement.value, // 3rd way of accessing data: local reference with Viewchild
        this.inputInfo.contact, // 1st way of accessing data: ngModel (data binding)
        this.inputInfo.address  // 1st way of accessing data: ngModel (data binding)
      ));

    
      // additional exercise solution: clear all fields after adding a friend
      inputName.value = "";
      this.inputEmail.nativeElement.value = "";
      this.inputInfo.contact = "";
      this.inputInfo.address = "";
    }
  }

}
