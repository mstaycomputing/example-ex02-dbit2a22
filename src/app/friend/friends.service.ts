import { Injectable, EventEmitter } from '@angular/core';
import { Friend } from "./friend.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "../../environments/environment";

// we need this variable from environment if we're not going to use proxy
// allows us to selectively indicate the URL of our Node.js server
// depending on whether we're in development or production
// in development, our Node.js server APIEndpoint will be http://localhost:3000
// see src/environments/environment.ts file
const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class FriendsService {

  // creates a "loudhailer" (EventEmitter) that emits empty message (void)
  // one EventEmitter for each event (added / deleted)
  friendAdded = new EventEmitter<void>();
  friendDeleted = new EventEmitter<void>();

  // the friendList in this service is private
  // hence other components won't be able to directly access/modify this list
  // this list will be synced up with the server by making a call to the server via loadFriends() method
  private friendList : Friend [] = [
    // new Friend(0, "John Lim", "john@gmail.com", "91234567", "Jurong East"),
    // new Friend(1, "Mary Tan", "mary@hotmail.com", "81234567", "Clementi")
  ];

  constructor(public httpClient: HttpClient) { }

  // public method that returns an Observable to make the server call
  // will execute GET call to retrieve list of Friends when subscribed to
  loadFriends() {
    return this.httpClient.get<Friend[]>(APIEndpoint + "/api/friends")
      .pipe( //  allows the Observable to "pass through" this pipe when executed
        map((friends) => { 
          // this will help to update the local friendList we have in the service
          this.friendList = friends;
          return friends; // we allow the original list of friends to be passed out of the pipe
        },
        (error) => {console.log(error);})
      );
  }

  // public method for other components to add a Friend into the list
  addFriend(newFriendInfo) {

    // preparing the headers for the server call
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    // wrapping the headers into an object (required format by the server)
    const options = {
      headers: httpHeaders
    }

    // making the server call to add the friend
    this.httpClient.post<Friend>(APIEndpoint + '/api/friends',
      {info : newFriendInfo}, options)
      .subscribe( (response) => { // immediate subscribe (trigger) of the Observable

        // the response is the Friend object that was added
        // we manually push it into the friendList we have in the service
        // so that we don't have to make another server call to sync up the friendList
        this.friendList.push(response);

        // ask the "loudhailer" (EventEmitter) to emit a message
        // to inform all parties that something has happened (a friend has been added)
        this.friendAdded.emit();
      });

    //// old code (when we don't have a server) ////////////
    // this.friendList.push(newFriendInfo);
    // this.friendAdded.emit();

    //// debug logs to prove that a friend was indeed added
    // console.log("INSIDE FRIENDS SERVICE");
    // console.log(this.friendList);
    ////////////////////////////////////////////////////////
    
  }

  // public method for other components to access a COPY of the friendList
  getFriends() {
    return this.friendList.slice();
  }

  //additional exercise: public method to delete a friend
  // must take in index (fr_id) of the friend to be deleted
  deleteFriend(index) {

    // making the server call to delete a friend with the corresponding index
    this.httpClient.delete<{"message":string}>(APIEndpoint + '/api/friend/' + index)
    .subscribe( (response) => { // immediate subscribe (trigger) of the Observable
      console.log(response);

      // making another server call to sync up the server's friend list with what we have in the service
      this.loadFriends().subscribe( (response) => {
        this.friendList = response;

        // alert everybody else that a friend has been deleted and the list updated
        // Friend Component will subscribe to this and handle the list display accordingly
        this.friendDeleted.emit();
      });
   
    });


    //// old code (when we don't have a server) /////////
    // this.friendList.splice(index, 1);
    // this.friendDeleted.emit();
    /////////////////////////////////////////////////////

  }

  // used for the /friend-detail/* page (to retrieve details of a single Friend)
  getFriend(fr_id) {

    // making the server call to get a friend with the corresponding index
    // it returns an Observable (whoever calls this function must subscribe() to trigger it)
    return this.httpClient.get<Friend>(APIEndpoint + '/api/friend/' + fr_id);

    //////////////// Old code (without server) ////////////////////////////////
    // perform a simple search for the Friend with the relevant fr_id
    // for (let friend of this.friendList) {
    //   if (friend.fr_id == fr_id) {
    //     return friend;
    //   }
    // }

    // if fr_id not found (means the above for loop finished running without returning a friend),
    // return undefined to the caller to indicate not found
    // return undefined;
    ///////////////////////////////////////////////////////////////////////////
  }

  updateFriend(updatedFriend: Friend) {
    // preparing the headers for the server call
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache'
    });

    // wrapping the headers into an object (required format by the server)
    const options = {
      headers: httpHeaders
    }

    // making the server call to put a friend with the corresponding index
    // call must made with a body and header to indicated content type is JSON
    // it returns an Observable (whoever calls this function must subscribe() to trigger it)
    return this.httpClient.put<{'success': boolean, 'message': string}>(APIEndpoint + "/api/friend/" + updatedFriend.fr_id,
                                                                {"info": updatedFriend}, options)
  }
}
