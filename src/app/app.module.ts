import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FriendComponent } from './friend/friend.component';
import { FriendAddComponent } from './friend/friend-add/friend-add.component';
import { FriendListComponent } from './friend/friend-list/friend-list.component';
import { HoverHighlightDirective } from './hover-highlight.directive';
import { ClickChangeSizeDirective } from './click-change-size.directive';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FriendDetailComponent } from './friend-detail/friend-detail.component';

import { ReactiveFormsModule } from "@angular/forms";
import { FindPipe } from './find.pipe';
import { AuthService } from './auth/auth.service';
import { HttpClientModule } from "@angular/common/http";
import { FriendEditComponent } from './friend/friend-edit/friend-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    FriendComponent,
    FriendAddComponent,
    FriendListComponent,
    HoverHighlightDirective,
    ClickChangeSizeDirective,
    HeaderComponent,
    PageNotFoundComponent,
    HomeComponent,
    ContactUsComponent,
    FriendDetailComponent,
    FindPipe,
    FriendEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
