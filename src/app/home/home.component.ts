import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  // a function to handle the "Log In" button click from the home HTML
  onLogin(username: HTMLInputElement, password: HTMLInputElement) {
    // asks the authService to check if login credentials from the form is valid
    // the service will process accordingly and either redirect user to /friend or do nothing
    this.authService.login(username.value, password.value);
  }

}
