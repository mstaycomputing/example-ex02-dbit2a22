import { Injectable } from '@angular/core';
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // a variable to keep track of whether the user is currently logged in
  loggedIn = false;

  constructor(private router: Router) { }

  // this is called from the home component
  login(username: string, password: string) {

    // hard-coded "user login" - we'll only accept ABC and 123 as valid credentials for now
    // will replace with server verification in later examples
    // this turns loggedIn variable to either true or false depending on whether credentials match
    this.loggedIn = (username == "ABC" && password == "123");

    console.log(this.loggedIn);

    // upon valid login, to redirect user to the /friend page
    if (this.loggedIn) {
      this.router.navigate(['/friend']);
    }
  }

  // this is called from the header component
  logout() {

    // manually set our variable to false to indicated logged out
    this.loggedIn = false;

    // throw the user back to home (login) page
    this.router.navigate(['/']);
  }

  // this is used in the auth-guard.service.ts / auth.guard.ts
  isAuthenticated() {

    // a Promise lets us execute an asynchronous call
    // in this case we are simulating a "server call" to verify login details
    // with delay of 1 second (1000 milliseconds)
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout( () => {

          // this Promise resolves to either true or false depending on whether user is logged in
          resolve(this.loggedIn);
        }, 1000);
      }
    );

    // by returning the promise,
    // we allow others a way to code actions to be performed AFTER the asynchronous call is resolved using .then(...)
    return promise;
  }
}
