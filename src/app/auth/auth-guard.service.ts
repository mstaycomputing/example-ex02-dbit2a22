import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public authService: AuthService, private router: Router) { }

  // a default interface from Angular that we implement to decide if a route can be activated (accessed)
  // these 2 lines of code are standard (can be generated via ng g g command)
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {

      // trigger the asynchronous "server" call to verify login
      return this.authService.isAuthenticated()
        .then((authenticated: boolean) => { // .then(...) allows us to handle things upon the completion of asynchronous call
          // check the resolve value from the call - a boolean that indicates if user is logged in
          if (authenticated) {
            // since user logged in, we allow the route in question to be activated
            return true;
          }
          else {
            // if authenticated is false, means user not logged in - throw user back to home (login) page
            console.log("Access Denied");
            this.router.navigate(['/']);
          }
        });
      }
}
