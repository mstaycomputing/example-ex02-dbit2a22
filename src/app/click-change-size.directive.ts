import { Directive, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appClickChangeSize]'
})
export class ClickChangeSizeDirective {
  // Additional exercise: a directive to change size of font upon clicking

  // We're using HostBinding with Input here (not using Renderer2)
  @HostBinding('style.fontSize') textSize: string;
  @Input('appClickChangeSize') newSize: string;

  // Alternative method to take in the new font size (similar to tutorial)
  // @Input('appClickChangeSize') newProperty: {fontSize: string};

  constructor() { }

  // "listen" / react to click event on the element that this directive is applied to
  @HostListener('click', ['$event']) mouseClick(eventData: Event) {
    this.textSize = this.newSize;
  }


}
