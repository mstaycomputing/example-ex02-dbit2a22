import { 
  Directive,
  ElementRef,
  Renderer2,
  OnInit,
  HostListener,
  HostBinding,
  Input 
} from '@angular/core';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

  // the background and text values will be input from the HTML
  // this makes our directive more dynamic (customisable)
  // so it doesn't always change to a background and text color we hard-coded hee
  @Input('appHoverHighlight') highlightColor: {
                                                background:string,
                                                text:string
                                              };
  
  // for the second way of changing style - via HostBinding                                            
  @HostBinding('style.color') textColor: string;
  @HostBinding('style.textDecoration') textDecoration: string;

  constructor(private renderer: Renderer2,
              private elRef: ElementRef) { }

  // "listen" / react to mouseenter event on the element that this directive is applied to
  @HostListener('mouseenter', ['$event']) mouseOver(eventData: Event) {
    // first way of changing style - via Renderer2
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 
                          this.highlightColor.background);

    // second way of changing style - via HostBinding (see @HostBinding above)
    this.textColor = this.highlightColor.text;
    this.textDecoration = 'line-through';
  }

  // "listen" / react to mouseleave event on the element that this directive is applied to
  @HostListener('mouseleave', ['$event']) mouseExit(eventData: Event) {
    // first way of changing style - via Renderer2
    this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'transparent');

    // second way of changing style - via HostBinding (see @HostBinding above)
    this.textColor = 'black';
    this.textDecoration = 'none';
  }

  //// old testing code: simple way of checking if your directive is used properly in the HTML
  //// it changes your target element's text color to blue upon loading
  // ngOnInit() {
  //   this.renderer.setStyle(this.elRef.nativeElement, 'color', 'blue');
  // }

}
