import { Component } from '@angular/core';
import { FriendsService } from './friend/friends.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FriendsService] // FriendsService is provided here so that the friendList won't be reset when we navigate out and back to the Friend page
})
export class AppComponent {
  title = 'My Friends';
}
